// ==UserScript==
// @name        vortex rage of bahamut port
// @namespace   http://userscripts.org/users/386397
// @description Rage of Bahamut ios/android app web port
// @include     http://bahamut-i.cygames.jp/bahamut_n/*
// @version     1
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_xmlhttpRequest
// @grant       GM_addStyle
// ==/UserScript==

init = new Date().getTime();

var $ = unsafeWindow.jQuery,	
	Rage = {
        "location": "",
		"storage": {
			"counter": GM_getValue('counter', 0),
			"autoquest": GM_getValue('autoquest', false),
			"cards": GM_getValue('cards', 0),
			"maxCards": GM_getValue('maxCards', 0)
		},
		"data": {
            "selector": {
                "remove": [
                    ".toppadding"
                    , ".bottompadding"
                ]
            },
            "css": {
                "bootstrap": [
                    "html{zoom:1;}",
                    "html,body,div#top{background:#222;font-family:'Helvetica', 'Tahoma', sans-serif !important;font-size:10px;}",
                    "body{width:auto;}",
                    "div#top{background:#111;border:2px solid #333;margin:20px auto 50px;border-radius:10px;}"
                ],
                "menu": {
                    "width" : "240px", 
                    "padding" : "5px 10px",
                    "position" : "fixed", 
                    "left" : "10px", 
                    "top" : "10px",
                    "background" : "#f3f3f3",
                    "border" : "1px solid #9f9f9f",
                    "border-radius" : "10px",
                    "box-shadow" : "0 0 15px rgba(0,0,0,0.15)"
				},
            },
			"time": init,
			"uri": window.location.pathname.replace(/^\/bahamut_n\//gi, ''),
		},
		"regexp": {
			"stamina": /^STAMINA:\s([0-9]*)\/\.*$/gi
		},
		"log": function (message) {
			console.log(message);
		},
        "error": function (message) {
            console.error(message);
        },
		"myPageCheck": function () {
			Rage.log('Current uri: "' + Rage.data.uri + '"');
				
			if (Rage.data.uri == "mypage") {
                Rage.location = "home";
				var timer = new Date().getTime(),
					timeout = 60000;

				window.setTimeout(function(){window.location.reload();}, timeout); // home page keep-alive
				
				window.setInterval(function(){Rage.log(Math.round((timer + timeout - new Date().getTime()) / 1000) + ' seconds left to refresh');}, 9999);
			}
				
			return Rage;
		},
        "getCardList": function () {
            if (Rage.location === "home") {
                var cardList = $('a[href="http://bahamut-i.cygames.jp/bahamut_n/card_list/index"]');
                
                if (cardList.length > 0) {
                    var cards = cardList.text().split("Cards")[1].split("/");
                    if (! isNaN(cards[0])) {
                        Rage.updateStorage("cards", cards[0]);
                    }
                    
                    if (! isNaN(cards[1])) {
                        Rage.updateStorage("maxCards", cards[1]);
                    }
                }
            }
            
            return Rage;
        },
        "updateStorage": function (key, value) {
            if (typeof(Rage.storage[key]) !== "undefined" && typeof(value) !== "undefined") {
                Rage.storage[key] = value;
                GM_setValue(key, value);
            }
        },
        "addFont": function () {
            /*var fontName = 'Ubuntu',
            	rules = document.createTextNode('body *, input[type="submit"], .page_container .lBtn { font-family: "' + fontName + '", "Arial", sans-serif !important;font-size:11px !important;line-height:12px !important; }'),
            	style = document.createElement('style'),
                link = document.createElement('link');
            
            style.styleSheet ? style.styleSheet.cssText = rules.nodeValue : style.appendChild(rules);
            
            link.media = 'all';
            link.type = 'text/css';
            link.href = '//fonts.googleapis.com/css?family=' + escape(fontName);
            link.rel  = 'stylesheet';
            
            $('head').append([link, style]);*/
            
            return Rage;
        },
        "addBootstrap": function () {
            GM_xmlhttpRequest({
                "method": "GET",
            	"url": "http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css?" + new Date().getTime(),
                "onload": function(response) {
                    if (response.responseText.length > 1000) {
                        var css = Rage.data.css.bootstrap.join('');
                        GM_addStyle(response.responseText + css);
                        $(Rage.data.selector.remove.join(',')).remove();
                        Rage.log('Loaded bootstrap + menu');
                    } else {
                        Rage.error('Failed loading bootstrap');
                    }
                }
            });
        
        	return Rage;
        },
		"init": function () {
            var time = new Date().getTime();
            
			Rage.myPageCheck()
            	.getCardList()
            	.addFont()
            	.addBootstrap();
				// .removeJunk()
				// .fixCss()
				// .getStamina();
			
			Rage.log('Loaded script in ' + ((time - Rage.data.time) / 1000) + '.' + (time - ((time - Rage.data.time) / 1000) * 1000) + ' seconds');
			Rage.log('This script has been run ' + Rage.storage.counter++ + ' times.');
            
            GM_setValue('counter', Rage.storage.counter);
		}
	};

$(document).ready(function(){
	Rage.init();
});